from flask import Flask, render_template, request
from sklearn import svm
import numpy as  np
import pickle
import sys

app = Flask(__name__, static_url_path='')

traffic_dataset = [[10, 0, 1, 8], [10, 0, 2, 9], [10, 1, 1, 13], [5, 0, 1, 15], [5, 1, 5, 16], [5, 1, 6, 18]]
traffic_status = [8, 7, 5, 6, 9, 9]
#clf = svm.SVR(gamma=0.001, C=100, )
#clf=pickle.load(open('trafficmodel2.pkl','rb'))



tdataset=np.loadtxt("generated8k.txt",dtype="float",delimiter=",",usecols=range(0,3))
print(tdataset)
opdataset=np.loadtxt("generated8k.txt",dtype="float",delimiter=",",usecols=3)
print(opdataset)
clf = svm.SVC(gamma=0.001, C=100, )
clf.fit(tdataset,opdataset)
@app.route('/')
def root():
    return app.send_static_file('index.html')

def modelprep():
    print("Called")
    print(clf is None)
    if 1==1:
        zz=open("Data/nheader.txt")
        czz=zz.readlines()
        ilist=[]
        trfc=[]
        for nn in czz:
            ce=nn.rstrip('\n').split(',')
            trfcs=ce.pop()
            ilist.append(ce)
            trfc.append(trfcs)
            print(ce)
        print("Done")
        print("RUnning model")
        clf.fit(ilist, trfc)
        


def insertData(road, day, time, status):
    try:
        global tdataset,opdataset
        temp = [int(road), int(day), int(time)]
        traffic_dataset.append(temp)
        ttemp=tdataset.tolist()
        ttemp.append(temp)
        otemp=opdataset.tolist()
        otemp.append(status)
        tadataset=np.array(ttemp)
        opadataset=np.array(otemp)
        tdataset=tadataset
        opdataset=opadataset
        clf.fit(tadataset, opadataset)
        print("Model re trained with data length ",len(opadataset))
        display = [int(road), int(day), int(time), int(status)]
        return str(display) + " Inserted to Traffic Dataset and Model re trained with data of length "+str(len(opadataset))
    except Exception as e:
        print(e)
        return "Some error occured while retraining Model "+str(e)

def predict_status(road, day, time):
    print("point 3")
    result = clf.predict([[int(road), int(day), int(time)]])
    print("point 4")
    print(type(result))
    return result


def clear_data():
    global traffic_dataset
    traffic_dataset = [[0, 0, 0, 0]]
    global traffic_status
    traffic_status = [0]


@app.route('/save',methods=['GET','POST'])
def save():
    road = request.args.get('road_id')
    direction = request.args.get('direction')
    day = request.args.get('dayOfWeek')
    time = request.args.get('timeOfDay')
    status = request.args.get('traffic_status')

    res = insertData(road, direction, day, time, status)
    return res


@app.route('/predict',methods=['GET','POST'])
def predict():
    try:
        road = request.args.get('road_id')
        direction = request.args.get('direction')
        day = request.args.get('dayOfWeek')
        time = request.args.get('timeOfDay')
        print("point 1")
        res = predict_status(road, direction, day, time)
        print("point 2")
        return res
    except Exception as e:
        print(e)


@app.route('/clear',methods=['GET','POST'])
def clear():
    clear_data()
    print(traffic_dataset)
    return "Traffic dataset cleared!"


@app.route('/predict_ui', methods=['GET', 'POST'])
def predict_ui():
    res = None
    #modelprep()
    print("Model loaded")
    iresponsescale=""
    icolor="red"
    if request.method == 'POST':
        road = request.form['road_id']
        #direction = request.form['direction']
        day = request.form['dayOfWeek']
        time = request.form['timeOfDay']
        if int(time)>24:
            return render_template('predict_ui.html', response="Invalid Time Please check the Time entered")
        if int(day)>6:
            return render_template('predict_ui.html', response="Invalid Date Please check the Date entered (0-6)")
        if int(road) >4 or int(road)==0:
            return render_template('predict_ui.html', response="Invalid Road, Please enter road number between 1 and 4")
        
        try:
            
            pres = predict_status(road, day, time)
            responsescale =""
            icolor="green"
            print(pres)
            ires=int(float(pres))
            if(ires<=5):
                iresponsescale="Very Low Traffic"
            if(ires>5) & (ires<=10):
                iresponsescale="Low Traffic"
            if(ires>10) & (ires<=15):
                iresponsescale="Medium Traffic"
                icolor="yellow"
            if(ires>15) & (ires<=30):
                iresponsescale="Moderate Traffic"
                icolor="orange"
            if(ires>30) & (ires<=50):
                iresponsescale="High Traffic"
                icolor="red"
            if(ires>50) & (ires<=100):
                iresponsescale="Very High Traffic"
                icolor="red"
            if(ires>100):
                iresponsescale="Congestion  -- Heavy Traffic"
                icolor="red"
            res="Estimated Traffic Status: " +str(ires)
            print(iresponsescale)
        except Exception as e:
            print(e)
            res="No Model trained or issue with server "+str(e)
            
    return render_template('predict_ui.html', response=res,responsescale=iresponsescale,trafficcolor=icolor)


@app.route('/insert_ui', methods=['GET', 'POST'])
def insert_ui():
    res = None
    if request.method == 'POST':
        road = request.form['road_id']
        #direction = request.form['direction']
        day = request.form['dayOfWeek']
        time = request.form['timeOfDay']
        status = request.form['traffic_status']
        try:
            res = insertData(road, day, time, status)
        except Exception as e:
            res =" Unable to insert data please verify the given data, Only Integers required"+str(e)
    return render_template('insert_ui.html', response=res)

@app.route('/test/', methods=['GET'])
def itest():
    clfz=svm.SVR(gamma=0.001, C=100, )
    #clfz=pickle.load(open('trafficmodel2.pkl','rb'))
    clfz.fit(traffic_dataset,traffic_status)
    return str(clfz.predict([[3,3,1,19]]))

if __name__ == '__main__':
    app.run(host='0.0.0.0')
